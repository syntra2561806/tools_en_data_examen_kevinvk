"""
Opdracht:

Bepalingen:
 - Je moet gebruik maken van de aangeleverde variable(n)
 - Je mag deze variable(n) niet aanpassen
 - Het is de bedoeling dat je op het einde 1 programma hebt
 - Als inlever formaat wordt een public git url verwacht die gecloned kan worden

/ 5 ptn 1 -(done) Maak een public repository aan op jouw gitlab/github account voor dit project
/10 ptn 2 - (done) Gebruik python om de gegeven joke_url aan te spreken
/10 ptn 3 - (done)Gebruik regex om de volgende data te extracten:
            - De eerste 20 tekens van de joke
/15 ptn 4 - (done)Verzamel onderstaande data en output alles als yaml. Een voorbeeld vind je hieronder.
/10 ptn 5 - (done)Doe een post naar de post_url.
            (done)- Stuur in de post een parameter genaamd "naam" mee met als waarde jouw voornaam
            (done)- Output het hele antwoord naar de terminal
            (done)- Haal uit het antwoord van de call jouw ip adres (origin) en geef dit weer met een f string

Totaal  /50ptn
"""

""" voorbeeld yaml output
jokes:
  - intro: <met regex extracted intro>
    joke: <joke>
    id: <joke id>
  - .. <tot aantal jokes bereikt is>
"""

joke_url = "https://icanhazdadjoke.com"
amount_of_jokes = 5

# https://httpbin.org/#/HTTP_Methods/post_post
post_url = "https://httpbin.org/post"

#posten op gitlab:
#git add .
#git commit -am "examen_kevinvk.py"
#git push

import requests
from datetime import datetime
import re
import yaml
from pprint import pprint
    
for i in range (5):
    def output(text):
        print(text)
    
    headers = {
        'Accept': 'application/json'
    }
        
    try:
        data = requests.get("https://icanhazdadjoke.com/", headers=headers)
        joke = data.json().get('joke')
        amount_of_joke = 5
    except:
        # standard joke here for if the random joke lookup fails
        joke = "What do clouds wear under their clothes? Thunderwear."
      
post_url = "https://httpbin.org/post"
response = requests.post(post_url, data={"naam": "kevin van keyenberg"})
output(response.text)

try:
    ip_address = response.json()['origin']
    output(f"Mijn IP address: {ip_address}")
except KeyError:
    output("geen ip address kunnen krijgen")
    
headers = {"Accept": "application/json"}

response = requests.get(joke_url, headers=headers)
regex_intro = r"(?P<intro>.{20})"
joke_data = response.json()
        
jokes = []

for x in range(amount_of_jokes):
    response = requests.get(joke_url, headers=headers)
    joke_data = response.json()
    joke_intro = re.match(r'(.{20})', joke_data['joke']).group()
    jokes.append({

        'intro': joke_intro,
        'joke': joke_data['joke'],
        'id': joke_data['id']

    })

output_data = {'jokes': jokes}
yaml_output = yaml.dump(output_data)

print(yaml_output)
output(joke)